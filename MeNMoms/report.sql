
use RETAILER_1924465102534813;

set @start_date:= @startDate*1000; 
set @end_date:=unix_timestamp(DATE_SUB(CURDATE(), INTERVAL 1 SECOND))*1000;



-- set @end_date:=@endDate*1000; 
set @fileName:='/var/lib/mysql-files/MnM_Reports_';
set @fromDate:=from_unixtime(floor(@startDate), '%Y-%m-%d');
set @toDate:=from_unixtime(floor(@endDate), '%Y-%m-%d');
set @finalFileName:=CONCAT(@fileName, @fromDate,CASE  WHEN @duration>1 THEN CONCAT("_to_",@toDate) ELSE CONCAT("","") end);
-- select "STORE_CODE","name","city","totalBills","taggedBills","taggedPercentage","customerEnrollCount","Repeat Customer",
--       "Repeat Bills","totalSales","loyltySales","Repeat sale","earn","Burn"

select "fromstart",@start_date from dual;
select "fromend", @end_date from dual;
select "start",@fromDate from dual;
select "end",@toDate from dual;
select "fileName",@fileName from dual;
select "duration", @duration from dual;
select "finalFileName", @finalFileName from dual; 

         select r1.STORE_ID storeId,sum(AMOUNT) as burn 
                from REDEMPTION r1 
                where r1.CREATION_DATE >= @start_date and 
                r1.CREATION_DATE <= @end_date 
                group by r1.STORE_ID; 

SET @sql_text = 
   CONCAT (' select "STORE CODE","NAME","CITY","TOTAL BILLS","TAGGED BILLS","TAGGED PERCENTAGE","CUSTIMER ENROLLMENT COUNT","New Transaction","REPEAT CUSTOMER", "REPEAT BILLS","TOTAL SALES",
        "LOYLTY SALES","REPEAT SALES","EARN","BURN" 
   UNION ALL 
  select 
  s.storeCode as storeCode ,s.name,s.city,tab5.totalBills,
  tab6.taggedBills,IFNULL(tab6.taggedBills/tab5.totalBills * 100,0),tab7.customerEnrollCount,IFNULL(repeatDetails.new_transactions,0),IFNULL(repeatDetails.rep_members,0),
IFNULL(repeatDetails.rep_transactions,0),
  tab5.totalSales,tab6.loyltySales,IFNULL(repeatDetails.rep_spends,0),tab8.earn,tab9.burn 
  from STORE s 
  join 
  -- all transcations
 (select s1.storeCode storeCode,IFNULL(tab1.totalBills,0) as totalBills,IFNULL(tab1.totalSales,0) as totalSales 
 from STORE s1 
   left join
 (
  select t1.STORE_CODE storeCode,count(*) as totalBills,sum(t1.TRANSACTION_AMOUNT) as totalSales 
 from TRANSACTION t1 
where t1.TRANSACTION_DATE >= @start_date and 
 t1.TRANSACTION_DATE <= @end_date and 
 (t1.CHANNEL="pos_transaction" or t1.CHANNEL="social_events") 
 group by t1.STORE_CODE) tab1 
 on 
 s1.storeCode=tab1.storeCode) tab5 

 on s.storeCode=tab5.storeCode 
 join 
                 -- tagged customers  transactions
(select s2.storeCode storeCode,IFNULL(tab2.taggedTotalBills,0) as taggedBills,
 IFNULL(tab2.loyltySales,0) as loyltySales 
 from STORE s2 
   left join 
 (
select t2.STORE_CODE storeCode,count(*) as taggedTotalBills,sum(t2.TRANSACTION_AMOUNT) as loyltySales 
 from TRANSACTION t2 
 where t2.TRANSACTION_DATE >= @start_date and 
 t2.TRANSACTION_DATE <= @end_date and t2.UNIQUE_CUSTOMER_ID !="9999999999" and 
 (t2.CHANNEL="pos_transaction" or t2.CHANNEL="social_events") 
 group by t2.STORE_CODE) tab2 
 on 
 s2.storeCode=tab2.storeCode
 ) tab6 
 on tab5.storeCode=tab6.storeCode 
join 
                -- all customers
(select s3.storeCode storeCode,IFNULL(tab3.customerEnrollCount,0) as customerEnrollCount  from STORE s3 
   left join 
 (
 select c1.STORE_CODE storeCode,count(*) as customerEnrollCount 
 from CUSTOMER c1 
 where c1.ENROLMENT_DATE >= @start_date and 
 c1.ENROLMENT_DATE <= @end_date 
 group by c1.STORE_CODE) tab3 
 on 
 s3.storeCode=tab3.storeCode
 )
 tab7 
 on 
 tab6.storeCode=tab7.storeCode 
  join 
  -- points
( select s4.storeCode storeCode,IFNULL(tab4.earn,0) as earn  from STORE s4 
   left join 
 ( 
	select p1.STORE_ID storeId,sum(DENOMINATION) as earn 
 		from POINT p1 
 			where p1.ACCRUAL_DATE >= @start_date and 
 			p1.ACCRUAL_DATE <= @end_date 
 			group by p1.STORE_ID) tab4 
 on 
 s4.storeId=concat("ObjectId(",tab4.storeId,")")) tab8 
 on tab7.storeCode=tab8.storeCode 
 join 
 ( select s5.storeCode storeCode,IFNULL(tab11.burn,0) as burn from STORE s5 
   left join 
 ( 
	 select r1.STORE_ID storeId,sum(AMOUNT) as burn 
 		from REDEMPTION r1 
 		where r1.CREATION_DATE >= @start_date and 
 		r1.CREATION_DATE <= @end_date 
		and REDEMPTION_STATE="BURNT" 
 		group by r1.STORE_ID) tab11 
 on 
 s5.storeId=concat("ObjectId(",tab11.storeId,")")) tab9 --Before s5.storeCode=concat("ObjectId(",tab11.storeId,")"))
 on tab8.storeCode=tab9.storeCode 
left join 
 retail_analytics.mnm_rep_t3 repeatDetails 
on tab9.storeCode=repeatDetails.Store_code 
 INTO OUTFILE "',@finalFileName,'.csv"  
FIELDS ENCLOSED BY "" 
TERMINATED BY "," 
ESCAPED BY "" 
LINES TERMINATED BY "\r\n" ');

select "query",@sql_text from dual;

PREPARE s1 FROM @sql_text;
EXECUTE s1;
DROP PREPARE s1;
select "end......" from dual;
